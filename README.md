# Spaces

Master
[![pipeline status](https://gitlab.com/kcohrs/unity-spaces/badges/master/pipeline.svg)](https://gitlab.com/kcohrs/unity-spaces/commits/master)
Develop
[![pipeline status](https://gitlab.com/kcohrs/unity-spaces/badges/develop/pipeline.svg)](https://gitlab.com/kcohrs/unity-spaces/commits/develop)



Unity Project for a 2D sandbox world simulation. CI realized with **[gableroux/unity3d docker image](https://hub.docker.com/r/gableroux/unity3d/)**, following the Proof of Concept by **[gableroux](https://gitlab.com/gableroux/unity3d-gitlab-ci-example)**. The latest `master` build can be found **[here](https://kcohrs.gitlab.io/unity-spaces/)**.


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Table of Contents  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Vision](#vision)
- [Getting started](#getting-started)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Vision
The goal is to create a dynamic world simulation, initially mainly focused on somewhat realistic landmass, climate and vegetation generation. Anything past that remains to be planned.

## Getting started
Developed with Unity3D version 2019.2, though compatibility with other versions is likely.

_In theory_, cloning the repo and importing it into a new Unity project should be sufficient for compiling and running.
