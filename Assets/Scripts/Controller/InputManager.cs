﻿using Spaces.Model;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

namespace Spaces.Renderer
{
    public class InputManager : MonoBehaviour
    {

        public TMP_InputField seedField;

        void Update()
        {

        }

        public void ResetWorld()
        {
            
            int newSeed;
            if (Int32.TryParse(seedField.text, out newSeed))
            {
                World.singleton.ResetWorld(newSeed);
            } else
            {
                World.singleton.ResetWorld();
            }

        }
    }

}
