﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spaces.Renderer;

namespace Spaces.Model
{
    public class World : MonoBehaviour
    {
        private Gridrenderer worldRender;
        public static World singleton;
        private static int worldHeight = 600;
        private static int worldWidth = 1000;
        public float worldScale = 10f;
        private int seed = 0;
        private WorldTile[,] worldmap;

        private bool worldchanged = false;

        public void Awake()
        {
            singleton = this;
            if (seed == 0)
            {
                seed = Random.Range(1, 10000);
            }
            worldmap = new WorldTile[worldWidth, worldHeight];
        }

        public void Update()
        {
            if (worldRender == null)
            {
                worldRender = Gridrenderer.singleton;
                if (worldRender != null)
                {
                    StartCoroutine(GenerateWorld());
                }
                   
            }
            else
            {
                if (worldchanged)
                {
                    StartCoroutine(worldRender.UpdateAllTiles(worldmap));
                    worldchanged = false;
                }
            }

        }

        public IEnumerator GenerateWorld()
        {
            yield return null;
            float[,] noisemap1 = GenerateNoiseMap(20f);
            float[,] noisemap2 = GenerateNoiseMap(500f);

            float[,] noisemap = new float[worldWidth, worldHeight];
            for (int i = 0; i < worldWidth; i++)
            {
                for (int j = 0; j < worldHeight; j++)
                {
                    noisemap[i,j] = (noisemap1[i, j] * 0.2f + noisemap2[i, j] );
                }
            }
                    //yield return null;
                    //randomize all water
                    for (int i = 0; i < worldWidth; i++)
            {
                for (int j = 0; j < worldHeight; j++)
                {
                    if(noisemap[i,j] > 0.8f)
                    {
                        worldmap[i, j] = new WorldTile(Terrain.Rock,noisemap[i, j]);
                    }
                    else if (noisemap[i, j] > 0.55f)
                    {
                        worldmap[i, j] = new WorldTile(Terrain.Grass, noisemap[i, j]);
                    }
                    else if (noisemap[i, j] > 0.5f)
                    {
                        worldmap[i, j] = new WorldTile(Terrain.Sand, noisemap[i, j]);
                    }
                    else {
                        worldmap[i, j] = new WorldTile(Terrain.Water, noisemap[i, j]);
                    }
                }
               // yield return null;
            }
            worldchanged = true;
          //  yield return null;
        }

        public float[,] GenerateNoiseMap(float scale)
        {
            float[,] noiseMap = new float[worldWidth, worldHeight];
            for (int i = 0; i < worldWidth; i++)
            {
                for (int j = 0; j < worldHeight; j++)
                {
                    float sampleX = ((i) + seed) / scale;
                    float sampleZ = ((j) + seed) / scale;
                    float noise = Mathf.PerlinNoise(sampleX, sampleZ);
                    noiseMap[i, j] = noise;
                }
            }
            return noiseMap;
        }

        public void ResetWorld(int newSeed = 0)
        {
            if (newSeed == 0)
            {
                seed = Random.Range(1, 10000);
            } else
            {
                seed = newSeed;
            }
            
            worldmap = new WorldTile[worldWidth, worldHeight];
            worldRender = null;
        }

    }

}

