﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spaces
{
    public enum Terrain
    {
        Void,
        Water,
        Sand,
        Grass,
        Rock,
        Mud
    }

    


    public struct WorldTile
    {
        public Terrain type;
        public float height;

        //add other information here
        public static Dictionary<Terrain, float> TerrainHeight = new Dictionary<Terrain, float>
        {
            { Terrain.Water, 0f},
            { Terrain.Sand, 0.5f},
            { Terrain.Mud, 0.55f },
            { Terrain.Rock, 0.8f },
             { Terrain.Grass, 0.55f }
        };

        public WorldTile(Terrain _t, float _h)
        {
            type = _t;
            height = _h;
        }


    }
}