﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spaces.Renderer
{
    public class CameraControl : MonoBehaviour
    {

        private Camera cam;
        public Vector2 zoomRange = new Vector2(5, 100);
        public float CurrentZoom = 10;
        public float ZoomZpeed = 1;
        public float ScrollSpeed = 15;
        public float ScrollEdge = 0.05f;
        public float PanSpeed = 10;

        public bool mouseEnabled = false;

        void Start()
        {
            cam = GetComponent<Camera>();
        }


        void Update()
        {
           
            // panning     
            if (Input.GetMouseButton(0))
            {
                /*
               transform.Translate(Vector3.right * Time.deltaTime * PanSpeed * (Input.mousePosition.x - Screen.width * 0.5f) / (Screen.width * 0.5f), Space.World);
                transform.Translate(Vector3.up * Time.deltaTime * PanSpeed * (Input.mousePosition.y - Screen.height * 0.5f) / (Screen.height * 0.5f), Space.World);
                */
            }

            else
            {
                if (Input.GetKey("d") || (mouseEnabled && Input.mousePosition.x >= Screen.width * (1 - ScrollEdge)))
                {
                    transform.Translate(Vector3.right * Time.deltaTime * PanSpeed, Space.Self);
                }
                else if (Input.GetKey("a") || (mouseEnabled && Input.mousePosition.x <= Screen.width * ScrollEdge))
                {
                    transform.Translate(Vector3.right * Time.deltaTime * -PanSpeed, Space.Self);
                }
                if (Input.GetKey("w") || (mouseEnabled && Input.mousePosition.y >= Screen.height * (1 - ScrollEdge)))
                {
                    transform.Translate(Vector3.up * Time.deltaTime * PanSpeed, Space.Self);
                }
                else if (Input.GetKey("s") || (mouseEnabled && Input.mousePosition.y <= Screen.height * ScrollEdge))
                {
                    transform.Translate(Vector3.up * Time.deltaTime * -PanSpeed, Space.Self);
                }
            }

            // zoom in/out
            CurrentZoom -= (Input.GetAxis("Mouse ScrollWheel") * Time.deltaTime * 1000 * ZoomZpeed);
            CurrentZoom = Mathf.Clamp(CurrentZoom, zoomRange.x, zoomRange.y);
            cam.orthographicSize = (int)CurrentZoom;

        }
    }

}
