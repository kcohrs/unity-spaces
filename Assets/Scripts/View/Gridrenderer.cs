﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Spaces.Renderer
{
    public class Gridrenderer : MonoBehaviour
    {
        //change this to hierachichal someday
        public static Gridrenderer singleton;

        private Canvas worldCanvas;
        private Image worldMap;

        void Awake()
        {
            singleton = this;

            worldCanvas = GetComponent<Canvas>();
            worldMap = GetComponentInChildren<Image>();

        }

        public IEnumerator UpdateAllTiles(WorldTile[,] worldmap)
        {
            yield return null;

            int Hoffset = worldmap.GetLength(0);
            int Woffset = worldmap.GetLength(1);

            Texture2D tex = new Texture2D(Hoffset, Woffset, TextureFormat.ARGB32, false);

            tex.filterMode = FilterMode.Point;

            for (int i = 0; i < Hoffset; i++)
            {
                for (int j = 0; j < Woffset; j++)
                {
                    tex.SetPixel(i, j, GetColorForTerrain(i, j, ref worldmap));
                }
            }
            tex.Apply();

            Sprite worldRender = Sprite.Create(tex, new Rect(0.0f, 0.0f, tex.width, tex.height), new Vector2(0.5f, 0.5f), 1f);

            worldMap.sprite = worldRender;
        }

        internal Color GetColorForTerrain(int x, int y, ref WorldTile[,] worldmap)
        {

            switch (worldmap[x,y].type) {
                case Terrain.Water:
                    return Color.Lerp(Color.blue, Color.black, 0.5f - (worldmap[x, y].height - WorldTile.TerrainHeight[Terrain.Water]));
                case Terrain.Mud:
                    return Color.Lerp(Color.magenta, Color.black, 0.5f - (worldmap[x, y].height - WorldTile.TerrainHeight[Terrain.Mud]));
                case Terrain.Rock:
                    return Color.Lerp(Color.gray, Color.black, 0.5f - (worldmap[x, y].height - WorldTile.TerrainHeight[Terrain.Rock]));
                case Terrain.Sand:
                    return Color.Lerp(Color.yellow, Color.black, 0.5f - (worldmap[x, y].height - WorldTile.TerrainHeight[Terrain.Sand]));
                case Terrain.Grass:
                    return Color.Lerp(Color.green, Color.black, 0.5f - (worldmap[x, y].height - WorldTile.TerrainHeight[Terrain.Grass]));
                case Terrain.Void:
                    return Color.green;
                default:
                    return Color.green;
            }
        }
    }
}

