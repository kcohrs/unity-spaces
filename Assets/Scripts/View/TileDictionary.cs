﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spaces
{
    [CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/TileDictionary", order = 1)]
    public class TileDictionary : ScriptableObject
    {
        public List<TileEntry> atlas;


        public TileEntry GetRandomByType(Terrain type)
        {
            List<TileEntry> tiles = atlas.FindAll(item => item.type == type);
            if (tiles.Count > 0)
            {
                return tiles[(int)Mathf.Floor(UnityEngine.Random.Range(0, tiles.Count))];
            }
            return TileEntry.Empty;
        }
    }
    [Serializable]
    public struct TileEntry
    {
        public string name;
        public Terrain type;
        public Sprite sprite;
        public Vector2 dimensions;
        public static TileEntry Empty { get { return new TileEntry("", Terrain.Void, null, Vector2.zero); } }

        public TileEntry(string _name, Terrain _type, Sprite _sprite, Vector2 _dimensions)
        {
            name = _name;
            type = _type;
            sprite = _sprite;
            dimensions = _dimensions;
        }
    }
}
